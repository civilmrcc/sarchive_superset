FROM apache/superset:4.0.2

# Switching to root to install the required packages
USER root

# The docker images come without any db driver installed,
# this installs the postgres driver
RUN pip install psycopg2-binary prophet

COPY superset_config.py /app/pythonpath

USER superset
