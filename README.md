# sarchive_superse
Apache Superset container for sarchive.
Apache doesn't publish docker images with postgres (or any) db drivers, so we need to create the image ourselves.

## Updating the image
Gitlab CI runs automatically when you merge commits into main.
The resulting build is published in the Gitlab container repository
and will be picked up by the watchtower service if configured. 

After the new image was pushed, you need to manually start the migration steps:
* Upgrade the database: `docker exec -it sarchive_superset superset db upgrade`
* Init the new version: `docker exec -it sarchive_superset superset init`


## Backup
Connect to postgres and then backup the database:
```bash
su postgres
cd ~
pg_dump -U postgres -d superset -F tar > /tmp/<date>_superset-<verion>_backup.tar
exit
```

Store the backup locally:
```bash 
scp root@host:/tmp/<date>_superset-<verion>_backup.tar ~/Dokumente/cmrcc/backup/  
```

## Build Docker Container
```bash
docker build . -t sarchive/superset:<desiredTag>
```

# Test Locally
```shell
docker run -d -t -i -e SUPERSET_SECRET_KEY='<super_secret_key>' \
	-e SUPERSET_SQLALCHEMY_DATABASE_URI='postgresql://<user>:<password>@<host:port>/<database>' \
	-e SUPERSET_MAPBOX_API_KEY='<your_key' \
	-p 8088:8088 \
	--name sarchive_superset \
	sarchive/superset:<desiredTag>
```
